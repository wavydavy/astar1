﻿using Newtonsoft.Json;
using System.Drawing;
using uk.wavydavy.Mapper;

namespace MapMaker
{
    internal class Program
    {
        static Map map;
        static int height, width;

        // These are the ARGB values of the colours
        // Only way to get the consts so they can be used in a switch statement
        const int Black = -16777216;
        //const int White = -1;
        const int Red = -65536;

        static void Main(string[] args)
        {
            string input, output;

            if (!ProcessParameters(args, out input, out output))
            {
                Usage();
                return;
            }

            Bitmap mapImage = new Bitmap(input);

            height = mapImage.Height; 
            width = mapImage.Width;

            map = new Map
            {
                MapDetail = new MapItems[height, width],
                Locations = new List<Mapper.Point>()
            }; 

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    Color currentPoint = mapImage.GetPixel(x, y);
                    switch (currentPoint.ToArgb())
                    {
                        case Black:
                            map.MapDetail[x, y] = MapItems.Wall;
                            break;
                        case Red:
                            map.MapDetail[x, y] = MapItems.Location;
                            map.Locations.Add(new Mapper.Point(x, y));
                            break;
                    }
                }
            }


            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    switch (map.MapDetail[x, y])
                    {
                        case MapItems.Wall:
                            Console.Write("+");
                            break;
                        case MapItems.Location:
                            Console.Write("!");
                            break;
                        default: Console.Write(" "); break;

                    }
                }
                Console.Out.WriteLine();
            }

            Console.Out.WriteLine(map.GetType().Name);

            Console.Out.WriteLine($"Saving map to {output}");

            JsonSerializer serializer = new JsonSerializer();

            using (Stream s = new FileStream(output, FileMode.Create, FileAccess.Write))
            using (TextWriter w = new StreamWriter(s))
            {
                serializer.Serialize(w, map);
            }
        }

        private static void Usage()
        {
            Console.WriteLine("Usage: MapMaker [-input inputfilename] [-output outputfilename]");
        }

        private static bool ProcessParameters(string[] args, out string input, out string output)
        {
            input = string.Empty;
            output = string.Empty;
            try
            {
                for (int i = 0; i < args.Length; i++)
                {
                    string arg = args[i].ToLower();

                    switch (args[i])
                    {
                        case "-input":
                            input = args[i + 1];
                            break;
                        case "-output":
                            output = args[i + 1];
                            break;
                        default:
                            Console.Out.WriteLine($"Unrecognised argument {arg}");
                            return false;
                    }

                    i++;
                }

                if (input == string.Empty)
                {
                    input = "map.png";
                }

                if (output == string.Empty)
                {
                    output = Path.GetFileNameWithoutExtension(input) + ".map";
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}