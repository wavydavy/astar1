﻿using System.Reflection.PortableExecutable;
using uk.wavydavy.Mapper;

namespace uk.wavydavy.astar
{
    public class Node : IComparable<Node>
    {
        public Node(int x, int y)
        {
            Location = new Point(x, y);
        }

        public Node(Point location)
        {
            Location = location;
        }

        public Node PreviousInPath { get; set; }

        public int X
        {
            get { return Location.X; }
        }

        public int Y
        {
            get { return Location.Y; }
        }

        public Point Location { get; private set; }

        public int GCost { get; set; }

        public int HCost { get; set; }

        public int FCost
        {
            get
            {
                return GCost + HCost;
            }
        }

        public override bool Equals(object? obj)
        {
            if (obj is Node other)
            {
                return X == other.X && Y == other.Y;
            }
            return false;
        }

        public int CompareTo(Node? other)
        {
            if (other == null) return 1;

            return FCost.CompareTo(other.FCost);
        }

        public static List<Point> Directions = new List<Point>
        {
            new Point(0, 1, 10),
            new Point(1, 1, 14),
            new Point(1, 0, 10),
            new Point(1, -1, 14),
            new Point(0, -1, 10),
            new Point(-1, -1, 14),
            new Point(-1, 0, 10),
            new Point(-1, 1, 14)
        };
    }
}
