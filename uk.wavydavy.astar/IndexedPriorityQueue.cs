﻿using System.Collections.Generic;
using System.Reflection;

namespace uk.wavydavy.astar
{
    public class IndexedPriorityQueue<TElement, TPriority> : PriorityQueue<TElement, TPriority>
    {
        private Dictionary<TElement, TElement> elements;

        public IndexedPriorityQueue() : base()
        {
            elements = new();
        }

        public void Enqueue(TElement element, TPriority priority)
        {
            base.Enqueue(element, priority);
            elements.Add(element, element);
        }

        public TElement Dequeue()
        {
            TElement element = base.Dequeue();
            elements.Remove(element);
            return element;
        }

        TElement this[TElement key]
        {
            get
            {
                return elements[key];
            }
        }

        public bool Contains(TElement element)
        {
            return elements.ContainsKey(element);
        }
    }
}