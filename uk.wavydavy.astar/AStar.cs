﻿using System.Runtime.CompilerServices;
using uk.wavydavy.Mapper;

namespace uk.wavydavy.astar
{
    public class AStar
    {
        public static bool TryFindPath(MapItems[,] grid, Node startNode, Node endNode, out Node endOfPath)
        {
            Dictionary<Point, Node> allNodes = new();

            endOfPath = null;
            startNode.GCost = 0;
            startNode.HCost = GetDistance(startNode, endNode);

            allNodes.Add(startNode.Location, startNode);
            allNodes.Add(endNode.Location, endNode);

            IndexedPriorityQueue<Node, int> openSet = new();
            Dictionary<Node, Node> closedSet = new();

            openSet.Enqueue(startNode, startNode.FCost);

            // This is to protect against impossible paths
            int beforeCount = openSet.Count;
            while (openSet.Count > 0)
            {
                Node currentNode = openSet.Dequeue();
                closedSet.Add(currentNode, currentNode);
                
                if(currentNode == endNode)
                {
                    endOfPath = currentNode;
                    return true;
                }

                foreach (var neighbour in GetNeighbours(grid, currentNode, endNode, allNodes)) 
                { 
                    if (closedSet.ContainsKey(neighbour))
                    {
                        continue;
                    }
                    if (neighbour.FCost < currentNode.FCost || !openSet.Contains(neighbour))
                    {
                        neighbour.PreviousInPath = currentNode;
                        if (!openSet.Contains(neighbour))
                        {
                            openSet.Enqueue(neighbour, neighbour.FCost);
                        }
                    }
                }

                if (beforeCount > openSet.Count)
                {
                    return false;
                }
            }

            // should never get here
            return false;
        }

        private static IEnumerable<Node> GetNeighbours(MapItems[,] grid, Node currentNode, Node endNode, Dictionary<Point, Node> allNodes)
        {
            foreach(var dir in Node.Directions)
            {
                int newX = currentNode.X + dir.X;
                int newY = currentNode.Y + dir.Y;

                if (newX >= 0 && newX < grid.GetLength(0)
                    && newY >= 0 && newY < grid.GetLength(1)
                    && grid[newX, newY] != MapItems.Wall)
                {
                    var newPoint = new Point(newX, newY);
                    if (!allNodes.ContainsKey(newPoint))
                    {
                        var newNode = new Node(newPoint);
                        newNode.HCost = GetDistance(newNode, endNode);
                        allNodes.Add(newPoint, newNode);
                    }
                    var neighbour = allNodes[newPoint];
                    neighbour.GCost = currentNode.GCost + dir.NeighbourDistance;
                    yield return neighbour;
                }
            }
        }

        private static int GetDistance(Node newNode, Node endNode)
        {
            int distance = ((newNode.X - endNode.X) ^ 2) + ((newNode.Y - endNode.Y) ^ 2);
            return Math.Abs(distance);
        }
    }
}
