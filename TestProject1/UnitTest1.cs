using Microsoft.VisualBasic;
using System.IO.Compression;
using System.Text;

namespace TestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void PathsNotEqual1()
        {
            Tuple<Tuple<ushort, ushort>,Tuple<ushort, ushort>> path1, path2;

            path1 = new Tuple<Tuple<ushort, ushort>, Tuple<ushort, ushort>>(new Tuple<ushort, ushort>(1, 1), new Tuple<ushort, ushort>(2, 2));

            path2 = new Tuple<Tuple<ushort, ushort>, Tuple<ushort, ushort>>(new Tuple<ushort, ushort>(1, 1), new Tuple<ushort, ushort>(2, 3));

            Assert.AreNotEqual(path1, path2);
        }
        [TestMethod]
        public void PathsEqual1()
        {
            Tuple<Tuple<ushort, ushort>, Tuple<ushort, ushort>> path1, path2;

            path1 = new Tuple<Tuple<ushort, ushort>, Tuple<ushort, ushort>>(new Tuple<ushort, ushort>(1, 1), new Tuple<ushort, ushort>(2, 2));

            path2 = new Tuple<Tuple<ushort, ushort>, Tuple<ushort, ushort>>(new Tuple<ushort, ushort>(1, 1), new Tuple<ushort, ushort>(2, 2));

            Assert.AreEqual(path1, path2);
        }

        [TestMethod]
        public void CompressionTest()
        {
            string stuff = "stuff";
            StringBuilder stringBuilder = new StringBuilder();
            byte[] buffer = new byte[1024];
            using (MemoryStream stream = new MemoryStream())
            using (GZipStream gzStream = new GZipStream(stream, CompressionLevel.NoCompression))
            {
                StreamWriter writer = new StreamWriter(gzStream);
                writer.Write(stuff);
                writer.Flush();
                stream.Position = 0;

                BinaryReader reader = new BinaryReader(stream);
                
                while(reader.Read(buffer, 0, buffer.Length) > 0)
                {
                    stringBuilder.Append(Convert.ToBase64String(buffer, 0, buffer.Length));
                }
            }

            

            //byte[] buffer2 = Convert.FromBase64String(stringBuilder.ToString());
            using (var inputStream = new MemoryStream(buffer))
            {
                using var outputStream = new MemoryStream();
                using (var gzip = new GZipStream(inputStream, CompressionMode.Decompress))
                {
                    gzip.CopyTo(outputStream);
                }
            }

            Assert.IsTrue(true);
        }
    }
}