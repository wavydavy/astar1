﻿using Newtonsoft.Json;
using uk.wavydavy.astar;
using uk.wavydavy.Mapper;

namespace AStar1
{
    internal class Program
    {
        static Dictionary<Tuple<Point, Point>, List<Node>> paths;
        static Map map;
        static int Height, Width;

        static void Main(string[] args)
        {
            if (!GetParameters(args, out string mapName))
            {
                Usage();
            }

            paths = new();

            map = LoadMap(mapName);
            Width = map.MapDetail.GetLength(0);
            Height = map.MapDetail.GetLength(1);

            DisplayMap();

            Point startLocation, endLocation;

            Random random = new Random();
            startLocation = map.Locations[random.Next(map.Locations.Count)];
            do
            {
                endLocation = map.Locations[random.Next(map.Locations.Count)];
            } while (startLocation == endLocation);

            Node startNode = new Node(startLocation);
            Node endNode = new Node(endLocation);
            if(!AStar.TryFindPath(map.MapDetail, startNode, endNode, out endNode))
            {
                Console.Out.WriteLine("No path found");
                return;
            }

            var currentNode = endNode;
            do
            {
                map.MapDetail[currentNode.X, currentNode.Y] = MapItems.Path;
                currentNode = currentNode.PreviousInPath;
            } while (currentNode.PreviousInPath != null);

            DisplayMap();
        }

        private static void DisplayMap()
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    switch (map.MapDetail[x, y])
                    {
                        case MapItems.Wall:
                            Console.Write("-");
                            break;
                        case MapItems.Location:
                            Console.Write("!");
                            break;
                        case MapItems.Path:
                            Console.Write("x");
                            break;
                        default: Console.Write(" "); break;

                    }
                }
                Console.Out.WriteLine();
            }
        }

        private static bool GetParameters(string[] args, out string mapName)
        {
            // default map name
            mapName = "map.map";
            try
            {
                for (int i = 0; i < args.Length; i++)
                {
                    string arg = args[i].ToLower();
                    switch (arg)
                    {
                        case "-filename":
                            mapName = args[i + 1];
                            break;
                        default:
                            Console.Out.WriteLine($"Unrecognised argument {arg}");
                            return false;
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private static void Usage()
        {
            Console.Out.WriteLine($"AStar1 [-filename mapfilename]");
        }

        private static Map LoadMap(string filename)
        {
            Map map;
            JsonSerializer serializer = new JsonSerializer();
            using (Stream s = new FileStream(filename, FileMode.Open, FileAccess.Read))
            using (TextReader tr = new StreamReader(s))
            using (JsonTextReader jr = new JsonTextReader(tr))
            {
                map = serializer.Deserialize<Map>(jr);
            }

            return map;
        }
    }
}