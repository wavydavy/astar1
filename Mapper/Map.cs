﻿using Newtonsoft.Json;
using System.IO.Compression;
using System.Text;

namespace uk.wavydavy.Mapper
{
    public class Map
    {
        public List<Point> Locations{ get; set; }

        public List<List<Point>> Paths { get; set; }

        public MapItems[,] MapDetail { get; set; }

        //public string CompressedDetails
        //{
        //    get
        //    {
        //        StringBuilder stringBuilder = new StringBuilder();

        //        using (var memStream = new MemoryStream())
        //        {
        //            var gzstream = new GZipStream(memStream, CompressionLevel.SmallestSize);
        //            TextWriter writer = new StreamWriter(gzstream);
                
        //            JsonSerializer js = new JsonSerializer();
        //            js.Serialize(writer, MapDetail);
        //            writer.Flush();
        //            memStream.Position = 0;

        //            BinaryReader reader = new BinaryReader(memStream);
        //            byte[] buffer = new byte[1024];
        //            while(reader.Read(buffer, 0, buffer.Length) > 0)
        //            {
        //                stringBuilder.Append(Convert.ToBase64String(buffer, 0, buffer.Length));
        //            }                    
        //        }
        //        return stringBuilder.ToString();
        //    }

        //    set
        //    {
        //        byte[] gzMapDetails = Convert.FromBase64String(value);
        //        //byte[] decompressed;
        //        using (var inputStream = new MemoryStream(gzMapDetails))
        //        {
        //            using var outputStream = new MemoryStream();
        //            using (var gzip = new GZipStream(inputStream, CompressionMode.Decompress))
        //            {
        //                gzip.CopyTo(outputStream);
        //            }

        //            //decompressed = outputStream.ToArray();
        //        }
        //        //JsonSerializer js = new JsonSerializer();
        //        //js.Deserialize()

        //        //byte[] gzMapDetails = Convert.FromBase64String(value);
        //        //using (var memStream = new MemoryStream(gzMapDetails))
        //        //using (var gzStream = new GZipStream(memStream, CompressionMode.Decompress))
        //        //using (var resultStream  = new MemoryStream())
        //        //{
        //        //    //gzStream.Write(gzMapDetails, 0, gzMapDetails.Length);
        //        //    //gzStream.Read(gzMapDetails, 0, gzMapDetails.Length);
        //        //    //gzStream.Flush();
        //        //    //memStream.Position = 0;

        //        //    gzStream.CopyTo(resultStream);
        //        //    gzStream.Flush();

        //        //    TextReader tr = new StreamReader(resultStream);
        //        //    JsonSerializer js = new JsonSerializer();
        //        //    MapDetail = (MapItems[,])js.Deserialize(tr, typeof(MapItems[,]));
        //        //}
        //    }
        //}
    }
}
