﻿using System.Runtime.Serialization;

namespace uk.wavydavy.Mapper
{
    public enum MapItems
    {
        None,
        Wall,
        Location,
        Path
    }
}