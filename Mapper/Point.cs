﻿using Newtonsoft.Json;

namespace uk.wavydavy.Mapper
{    
    public class Point
    {

        [JsonConstructor()]
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Point(int x, int y, int neighbourDistance) : this(x, y)
        {
            NeighbourDistance = neighbourDistance;
        }

        public int X { get; }
        public int Y { get; }

        public int NeighbourDistance { get; private set; }

        public override bool Equals(object? obj)
        {
            if (obj == null) return false;

            if (obj is Point other)
            {
                return X == other.X && Y == other.Y;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y);
        }
    }
}
